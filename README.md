## BashyPing

A ping utiltiy for swift/iOS with output similar to BSD ping. This is a swift wrapper around Apple's open source SimplePing class.

Written with Swift 5 on iOS 12.4. 


#### Demo:
Clone this repo and run the demo app:

#### Framwork:

````
platform :ios, '11.0'
target 'BashyPingPodTest' do
    pod 'BashyPing', :git => 'https://gitlab.com/zakkhoyt/bashyping', :branch => 'master'
end
````
