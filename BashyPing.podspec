
Pod::Spec.new do |s|
    s.name = "BashyPing"
    s.version = "1.1.0"
    s.summary = "A ping utility for Swift on iOS"
    s.author = { "Zakk Hoyt" => "vaporwarewolf@gmail.com" }
    s.license = { :type => 'BSD' }
    s.homepage = "https://gitlab.com/zakkhoyt/bashyping"
    s.platforms = {
        :ios => 11.0
    }
    s.source = {
        :git => 'https://gitlab.com/zakkhoyt/bashyping.git',
        :tag =>  "#{s.version}"
    }
    s.source_files = 'BashyPing/**/*.{swift,h,m}'
    # s.ios.source_files = 'BashyPing/**/*.{swift,h,m}'
    s.requires_arc = true
    s.ios.deployment_target = '11.0'
    #  s.swift_version = 5.0
end
