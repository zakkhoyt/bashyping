//
//  BashyPing.h
//  BashyPing
//
//  Created by Zakk Hoyt on 9/25/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BashyPing.
FOUNDATION_EXPORT double BashyPingVersionNumber;

//! Project version string for BashyPing.
FOUNDATION_EXPORT const unsigned char BashyPingVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BashyPing/PublicHeader.h>

#import <BashyPing/SimplePing.h>
