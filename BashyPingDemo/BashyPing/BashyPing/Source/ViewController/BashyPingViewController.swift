//
//  PingViewController.swift
//  PingTest
//
//  Created by Zakk Hoyt on 9/3/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

public class BashyPingViewController: UIViewController {
    
    private enum State: Int {
        case stopped
        case started
    }
    
    private var state = State.stopped {
        didSet {
            let options: UIView.AnimationOptions = [.transitionCrossDissolve, .curveEaseInOut, .allowAnimatedContent, .layoutSubviews, .showHideTransitionViews]
            switch state {
            case .stopped:
                UIView.transition(from: stopView, to: startView, duration: 0.3, options: options, completion: nil)
            case .started:
                UIView.transition(from: startView, to: stopView, duration: 0.3, options: options, completion: nil)
            }
        }
    }
    
    var initialHost: String?
    private var ping: BashyPing?
    private var hasLoggedFirstRequest = false
    
    @IBOutlet weak var ipTextField: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet var startView: UIView!
    @IBOutlet var stopView: UIView!
    @IBOutlet weak var hostButton: UIButton!
    @IBOutlet weak var pingButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var doneBarb: UIBarButtonItem!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        clear()
        
        if let initialHost = initialHost {
            ipTextField.text = initialHost
            start()
        }
    }
    
    @IBAction func pingButtonTouchUpInside(_ sender: UIButton) {
        start()
    }
    
    @IBAction func stopButtonTouchUpInside(_ sender: Any) {
        stop()
    }
    
    @IBAction func hostButtonTouchUpInside(_ sender: Any) {
        stop()
        
        let vc = PingHostsTableViewController(nibName: nil, bundle: nil)
        vc.hostSelected = { [weak self] (host) in
            guard let safeSelf = self else { return }
            safeSelf.ipTextField.text = host
            safeSelf.start()
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func shareBarbAction(_ sender: UIBarButtonItem) {
        guard let text = ipTextField.text else { return }
        let vc = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func doneBarbAction(_ sender: Any) {
        // Must stop before we dismiss (else pinger will keep on!)
        stop()
        
        // If in nav controller and is root VC, dismiss the NC. If not root pop. If not in NC, dismiss
        if let nc = navigationController {
            if let _ = navigationController?.viewControllers.first as? BashyPingViewController {
                nc.dismiss(animated: true, completion: nil)
            } else {
                navigationController?.popViewController(animated: true)
            }
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
}

extension BashyPingViewController {
    private func clear() {
        textView.text = ""
    }
    private func appendLine(line: String) {
        let existingText = textView.text ?? ""
        textView.text = "\(existingText)\n\(line)"
        scrollTextViewToBottom(textView: textView)
    }
    private func scrollTextViewToBottom(textView: UITextView) {
        if textView.text.count > 0 {
            let location = textView.text.count - 1
            let bottom = NSMakeRange(location, 1)
            textView.scrollRangeToVisible(bottom)
        }
    }
}

extension BashyPingViewController {
    private func start() {
        stop()
        
        // Gather host
        guard let host = ipTextField.text,
            host.count > 0 else {
                appendLine(line: "Invalid host/ipAddress")
                return
        }
        navigationItem.title = host
        PingDefaults.writeHost(host)
        
        ping = BashyPing()
        ping?.delegate = self
        
        let configuration = BashyPing.Configuration(host: host,
                                                    repeatStyle: .infinite,
                                                    duration: 0.5)
        ping?.startPing(configuration: configuration)

        clear()
        state = .started
    }
    
    private func stop() {
        ping?.stopPinging()
        ping = nil
        hasLoggedFirstRequest = false
        state = .stopped
    }
}

extension BashyPingViewController: BashyPingDelegate {
    public func bashyPing(_ bashyPing: BashyPing,
                                 didSendPingRequest request: BashyPing.Request) {
        func updateOnMainQueue() {
            // IN order to match the output of `ping host` We only want to log this once per ping session
            guard !hasLoggedFirstRequest else { return }
            appendLine(line: request.description)
            hasLoggedFirstRequest = true
        }
        
        DispatchQueue.main.async {
            updateOnMainQueue()
        }
    }
    
    public func bashyPing(_ bashyPing: BashyPing,
                                 result: Result<BashyPing.Response, Error>) {
        func updateOnMainQueue() {
            switch result {
            case .success(let response):
                appendLine(line: response.description)
            case .failure(let error):
                appendLine(line: error.localizedDescription)
            }
        }
        
        DispatchQueue.main.async {
            updateOnMainQueue()
        }
    }
    
    public func bashyPingPingComplete(_ bashyPing: BashyPing,
                                             statistics: BashyPing.Statistics) {
        func updateOnMainQueue() {
            stop()
            appendLine(line: statistics.description)
        }
        
        DispatchQueue.main.async {
            updateOnMainQueue()
        }
    }
}

extension BashyPingViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        start()
        textField.resignFirstResponder()
        return false
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print(#function)
        return true
    }
}


