//
//  PingDefaults.swift
//  PingTest
//
//  Created by Zakk Hoyt on 9/3/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation


struct PingDefaults {
    static let hostDefaults = UserDefaults(suiteName: "Hosts")
    
    private static let HostsKey = "Hosts"
    
    static func readHosts() -> [String] {
        guard let hostDefaults = hostDefaults else { return [] }
        guard let data = hostDefaults.object(forKey: HostsKey) as? Data else {
            return []
        }
        do {
            let hosts = try JSONDecoder().decode([String].self, from: data)
            return hosts
        } catch {
            assertionFailure()
            return []
        }

    }
    
    @discardableResult
    static func writeHost(_ host: String) -> Bool {
        guard let hostDefaults = hostDefaults else { return false }
        var hosts: [String] = readHosts()
        
        if hosts.contains(where: {
            return $0 == host
        }) {
            print("hostDefaults already contains host: \(host)")
            return true
        }
        
        hosts.append(host)
        
        do {
            let data = try JSONEncoder().encode(hosts)
            hostDefaults.set(data, forKey: HostsKey)
            return hostDefaults.synchronize()
        } catch {
            assertionFailure()
            return false
        }
    }
    
    @discardableResult
    static func writeHosts(_ hosts: [String]) -> Bool {
        guard let hostDefaults = hostDefaults else { return false }
        do {
            let data = try JSONEncoder().encode(hosts)
            hostDefaults.set(data, forKey: HostsKey)
            return hostDefaults.synchronize()
        } catch {
            assertionFailure()
            return false
        }
    }

    
    @discardableResult
    static func deleteHost(_ host: String) -> Bool {
        var hosts = readHosts()
        hosts.removeAll { $0 == host }
        return writeHosts(hosts)
    }

    @discardableResult
    static func deleteAllHost() -> Bool {
        return writeHosts([])
    }


}
