//
//  PingDefaultsTableViewController.swift
//  PingTest
//
//  Created by Zakk Hoyt on 9/4/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

class PingHostsTableViewController: UITableViewController {

    private var hosts = PingDefaults.readHosts()
    
    var hostSelected: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    private func setupUI() {
        let bundle = Bundle.init(for: PingHostTableViewCell.self)
        let nib = UINib(nibName: PingHostTableViewCell.identifier, bundle: bundle)
        tableView.register(nib, forCellReuseIdentifier: PingHostTableViewCell.identifier)
        tableView.reloadData()
    }
    

    // MARK: - IBActions
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hosts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PingHostTableViewCell.identifier, for: indexPath) as! PingHostTableViewCell
        cell.textLabel?.text = hosts[indexPath.row]
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let host = hosts[indexPath.row]
        hostSelected?(host)
        navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }

    @available(iOS 11.0, *)
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        var actions: [UIContextualAction] = []
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (action, view, completion) in
            guard let safeSelf = self else { return }
            let host = safeSelf.hosts[indexPath.row]
            guard PingDefaults.deleteHost(host) else {
                completion(false)
                return
            }
            safeSelf.hosts = PingDefaults.readHosts()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            completion(true)
        }
        actions.append(deleteAction)
        
        return UISwipeActionsConfiguration(actions: actions)
    }

}
