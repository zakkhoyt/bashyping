//
//  PingHostTableViewCell.swift
//  PingTest
//
//  Created by Zakk Hoyt on 9/4/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

class PingHostTableViewCell: UITableViewCell {

    static let identifier = "PingHostTableViewCell"
}
