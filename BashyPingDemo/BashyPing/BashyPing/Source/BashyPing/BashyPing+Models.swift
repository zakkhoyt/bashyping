//
//  BashyPing+Helpers.swift
//  PingTest
//
//  Created by Zakk Hoyt on 9/25/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension BashyPing {
    public struct Configuration {
        public enum RepeatStyle {
            case once
            case finite(Int)
            case infinite
        }
        let host: String
        let repeatStyle: BashyPing.Configuration.RepeatStyle
        let duration: TimeInterval
        var allowUnexpectedPackets: Bool
        var counter = 0
        
        
        init(host: String,
             repeatStyle: BashyPing.Configuration.RepeatStyle,
             duration: TimeInterval,
             allowUnexpectedPackets: Bool = true) {
            self.host = host
            self.repeatStyle = repeatStyle
            self.duration = duration
            self.allowUnexpectedPackets = allowUnexpectedPackets
        }
    }

    public struct Request: CustomStringConvertible {
        let date = Date()
        let host: String
        let ipAddress: String?
        let payloadSize: Int
        let sequenceNumber: Int
        
        public var description: String {
            // "PING google.com (172.217.164.110): 56 data bytes"
            let ipString: String = {
                guard let ipAddress = ipAddress else { return " " }
                return " (\(ipAddress)) "
            }()
            return "PING \(host)\(ipString): \(payloadSize) data bytes"
        }
    }

    public struct Response: CustomStringConvertible {
        public let host: String
        public var elapsed: TimeInterval
        public var payloadSize: Int
        public var sequenceNumber: Int
        
        public var responseIpAddress: String?
        
        private var elapsedString: String {
            return String(format: "%0.3fms", elapsed)
        }
        
        public var description: String {
            // 64 bytes from 172.217.164.110: icmp_seq=0 ttl=52 time=12.751 ms
            return "\(payloadSize) bytes from \(responseIpAddress ?? host): icpm_seq=\(sequenceNumber) time=\(elapsedString)"
        }
    }
    
    public class Statistics: CustomStringConvertible {
        public let host: String
        public internal(set) var packetsReceived = 0
        public internal(set) var packetsSent = 0
        
        public internal(set) var minimum: TimeInterval?
        public internal(set) var average: TimeInterval?
        public internal(set) var maximum: TimeInterval?
        public internal(set) var standardDeviation: TimeInterval?
        
        internal init(host: String) {
            self.host = host
        }
        
        private var durations: [TimeInterval] = []
        
        internal func didSendPacket() {
            packetsSent += 1
        }
        
        internal func didReceivePacket(duration: TimeInterval) {
            packetsReceived += 1
            self.durations.append(duration)
            self.minimum = durations.min()
            self.maximum = durations.max()
            self.average = durations.average()
            self.standardDeviation = durations.standardDeviation()
        }
        
        public var description: String {
            //
            //            --- google.com ping statistics ---
            //        4 packets transmitted, 4 packets received, 0.0% packet loss
            //        round-trip min/avg/max/stddev = 14.747/21.480/28.137/5.505 ms
            
            let packetsLost = packetsSent - packetsReceived
            let percent = Float(packetsLost / (packetsSent > 0 ? packetsSent : 1)) / 100
            let percentString = String(format: "%.1f%", percent)
            return """
            
            \t--- \(host) ping statistics ---
            \(packetsSent) packets transmitted, \(packetsReceived) packets received, \(percentString) packet loss
            round-trip min/avg/max/stddev = 14.747/21.480/28.137/5.505 ms
            """
            
        }
    }
    
    internal struct Timeout {
        var timer: Timer?
        var duration: TimeInterval = 3
        var startDate = Date()
    }
}
