//
//  Array+Math.swift
//  PingTest
//
//  Created by Zakk Hoyt on 9/25/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Array where Element: FloatingPoint {
    func sum() -> Element {
        return self.reduce(0, +)
    }
    
    func average() -> Element? {
        guard self.count > 0 else { return nil }
        return self.sum() / Element(self.count)
    }
    
    func standardDeviation() -> Element? {
        guard let mean = self.average() else { return nil }
        let v = self.reduce(0, { $0 + ($1-mean)*($1-mean) })
        return sqrt(v / (Element(self.count) - 1))
    }
}
