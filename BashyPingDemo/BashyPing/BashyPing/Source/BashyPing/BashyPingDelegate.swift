//
//  BashyPingDelegate.swift
//  PingTest
//
//  Created by Zakk Hoyt on 9/25/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public protocol BashyPingDelegate: class {
    func bashyPing(_ bashyPing: BashyPing,
                   didSendPingRequest request: BashyPing.Request)
    
    func bashyPing(_ bashyPing: BashyPing,
                   result: Result<BashyPing.Response, Error>)
    
    func bashyPingPingComplete(_ bashyPing: BashyPing,
                               statistics: BashyPing.Statistics)
}
