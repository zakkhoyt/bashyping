//
//  BashyPing.swift
//
//  Created by Zakk Hoyt 2019
//
//

import Foundation

public class BashyPing: NSObject {
    
    public private(set) var configuration: Configuration = Configuration(host: "www.google.com",
                                                                         repeatStyle: .once,
                                                                         duration: 1)
    public private(set) var statistics: Statistics?
    
    public weak var delegate: BashyPingDelegate?
    
    
    private var nextPingTimer: Timer?
    private var isFinished: Bool = false
    
    private var timeout = Timeout()
    private var pinger: SimplePing!
    
    deinit {
        // To avoid telling the delegate that the ping is complete
        statistics = nil
        stopPinging()
        pinger = nil
    }

    /**
     Start a ping process
     
     - Parameter oneShot: Ping once or multiple. If multiple, caller must call stopPinging() to end the process
     - Parameter host: The hostname or IP to ping
     - Parameter timeout: Duration in seconds to wait for a response. If none is received an error will be returned through the delegate
     */

    public func startPing(configuration: Configuration) {
        
        self.configuration = configuration
        self.statistics = Statistics(host: configuration.host)
        
        self.pinger = SimplePing(hostName: configuration.host)
        self.pinger.delegate = self
        self.pinger.start()
    }

    /**
     Stop pinging
     
     - Parameter oneShot: Ping once or multiple. If multiple, caller must call stopPinging() to end the process
     - Parameter host: The hostname or IP to ping
     - Parameter timeout: Duration in seconds to wait for a response. If none is received an error will be returned through the delegate
     */

    public func stopPinging() {

        self.isFinished = true
        
        pinger?.stop()
 
        nextPingTimer?.invalidate()
        nextPingTimer = nil
        
        timeout.timer?.invalidate()
        timeout.timer = nil
        
        if let statistics = statistics {
            delegate?.bashyPingPingComplete(self, statistics: statistics)
            self.statistics = nil
        }
    }
    
    // MARK - Private methods
    
    private func processPingResponse() {
        // We received our response. Now what?
        switch configuration.repeatStyle {
        case .once:
            stopPinging()
        case .infinite:
            scheduleNextPing()
        case .finite(let counter):
            guard configuration.counter < counter - 1 else {
                stopPinging()
                return
            }
            configuration.counter += 1
            scheduleNextPing()
        }
        
    }
    
    private func scheduleNextPing() {
        nextPingTimer = Timer.scheduledTimer(withTimeInterval: configuration.duration,
                                             repeats: false,
                                             block: { [weak self] (_) in
                                                guard let safeSelf = self else { return }
                                                if !safeSelf.isFinished {
                                                    safeSelf.pinger.send(with: nil)
                                                }
        })
    }
    
    private func scheduleResponseTimeout() {
        func timerFired() {
            let userInfo: [String: Any] = [
                NSLocalizedDescriptionKey: "Ping timed out. Hostname or address not reachable, or network is powered off",
                NSLocalizedFailureReasonErrorKey: "Please check the hostname, address, and internet reachability"
            ]
            let err = NSError(domain: "com.bashyPing.unexpected", code: -100, userInfo: userInfo)
            delegate?.bashyPing(self, result: Result{ throw err })
            stopPinging()
        }

        timeout.timer?.invalidate()
        timeout.timer = nil
        timeout.startDate = Date()
        timeout.timer = Timer.scheduledTimer(withTimeInterval: timeout.duration,
                                             repeats: false, block: { [weak self] _ in
                                                guard let safeSelf = self else { return }
                                                if !safeSelf.isFinished {
                                                    timerFired()
                                                }
        })
    }
}

// MARK: - Simple Ping Delegatesb
extension BashyPing: SimplePingDelegate {
    
    public func simplePing(_ pinger: SimplePing,
                           didStartWithAddress address: Data) {
        print(#function)
        
        // Address resolved. We can now send the first ping request.
        // See more delegate methods for follow up pings
        pinger.send(with: nil)
    }
    
    public func simplePing(_ pinger: SimplePing,
                           didSendPacket packet: Data,
                           host: String,
                           sequenceNumber: UInt16) {
        if !isFinished {
            print(#function)
            statistics?.didSendPacket()
            let request = Request(host: host, ipAddress: nil, payloadSize: packet.count, sequenceNumber: Int(sequenceNumber))
            delegate?.bashyPing(self, didSendPingRequest: request)
            scheduleResponseTimeout()
        }
    }
    
    public func simplePing(_ pinger: SimplePing,
                           didReceivePingResponsePacket packet: Data,
                           host: String,
                           ipAddress: String,
                           sequenceNumber: UInt16) {
        
        if !isFinished {
            print(#function)
            let response = Response(host: host,
                                    elapsed: Date().timeIntervalSince(timeout.startDate),
                                    payloadSize: packet.count,
                                    sequenceNumber: Int(sequenceNumber),
                                    responseIpAddress: ipAddress)
            statistics?.didReceivePacket(duration: response.elapsed)
            delegate?.bashyPing(self, result: Result{ response })
            processPingResponse()
        }
    }

    public func simplePing(_ pinger: SimplePing,
                           didReceiveUnexpectedPacket packet: Data,
                           host: String,
                           ipAddress: String,
                           sequenceNumber: UInt16) {

        if !isFinished {
            print(#function)
            if configuration.allowUnexpectedPackets {
                // The lamp tends to get into a state where it sends this sort of thing until some sort of reset
                let response = Response(host: host,
                                        elapsed: Date().timeIntervalSince(timeout.startDate),
                                        payloadSize: packet.count,
                                        sequenceNumber: Int(-1),
                                        responseIpAddress: ipAddress)
                delegate?.bashyPing(self, result: Result{ response })
            } else {
                let error = NSError(domain: "com.bashyPing.unexpected",
                                    code: -101,
                                    userInfo: [NSLocalizedDescriptionKey: "Did receive unexpected packet (\(packet.count) bytes) from \(ipAddress)"])
                if !isFinished {
                    delegate?.bashyPing(self, result: Result{ throw error })
                }
                stopPinging()
            }
        }
    }
    
    public func simplePing(_ pinger: SimplePing,
                           didFailToSendPacket packet: Data,
                           host: String,
                           sequenceNumber: UInt16,
                           error: Error) {
        
        if !isFinished {
            print(#function)
            delegate?.bashyPing(self, result: Result{ throw error })
        }
        stopPinging()
    }
    
    public func simplePing(_ pinger: SimplePing, didFailWithError error: Error) {
        if !isFinished {
            print(#function)
            delegate?.bashyPing(self, result: Result{ throw error })
        }
        stopPinging()
    }
}
